var whiteboardApp = new Vue({
  el: "#whiteboardApp",
  data: {
    username: "",
    loaded: false,
    createWhiteboard: false,
    columns: [],
    whiteboardName: "",
    whiteboards: {},
    sharedWhiteboards: {},
    notifications: null,
    showNotifications: false,
    currentNotificationTimeStamp: 0
  },
  methods: {
    openEditor: function() {
      whiteboardApp.createWhiteboard = true;
    },
    closeEditor: function() {
      whiteboardApp.createWhiteboard = false;
      whiteboardApp.columns = [];
      whiteboardApp.whiteboardName = "";
    },
    addColumn: function() {
      whiteboardApp.columns.push("");
    },
    removeColumn: function(column) {
      whiteboardApp.columns.splice(column, 1);
    },
    createNewWhiteboard: function () {
      var whiteboardName = whiteboardApp.whiteboardName;
      var columns = whiteboardApp.columns.reduce(function (acc, val) {
        return acc+","+val;
      }, "");
      if (whiteboardApp.columns.length>0)
        columns = columns.substring(1, columns.length);

      var query = {"tableName" : whiteboardName, "columns": columns};

      axios.get("/createNewWhiteboard", {params:query}).
      then(function (response) {
        window.location.href = "/";
      });
    },
    toggleNotifications: function () {
      whiteboardApp.showNotifications = !whiteboardApp.showNotifications;
    },
    openNotification: function (notification_id, whiteboard_id) {
      axios.get("/closeNotifications", {params:{"notification_ids":notification_id}}).then(function (response) {
        location.href="board?id="+whiteboard_id;
      });
    },
    getNotifications: function(whenFound) {
      axios.get("/getNotifications")
      .then(function (response) {
        whenFound(response.data);
      });
    },
    pingServerForNotifications: function (whenFound) {
      axios.get("/getNotificationTimestamp").then(function (response) {
        whenFound(response.data);
      });
    },
    lookForNewNotifications: function() {
      whiteboardApp.pingServerForNotifications(function (timestamp) {
        if (whiteboardApp.currentNotificationTimeStamp < timestamp) {
          whiteboardApp.currentNotificationTimeStamp = timestamp;

          whiteboardApp.getNotifications(function (notifications) {
            whiteboardApp.notifications = notifications;
          })
        }

        setTimeout(whiteboardApp.lookForNewNotifications, 1000);
      });
    }
  }
});

axios.get("/currentSession").then(function (response) {
  whiteboardApp.username = response.data.username;
});

axios.get("/getWhiteboards").then(function (response) {
  whiteboardApp.loaded = true;
  whiteboardApp.whiteboards = response.data;
});

axios.get("/getSharedWhiteboards").then(function (response) {
  whiteboardApp.sharedWhiteboards = response.data;
});

whiteboardApp.getNotifications(function (notifications) {
  whiteboardApp.notifications = notifications;
})

whiteboardApp.pingServerForNotifications(function (timestamp) {
  whiteboardApp.currentNotificationTimeStamp = timestamp;
  whiteboardApp.lookForNewNotifications();
});

document.getElementById("whiteboardApp").classList.remove("initial");
