var boardApp = new Vue({
  el:"#boardApp",
  data: {
    whiteboard: null,
    sharedWhiteboard: null,
    loaded: false,
    addField: false,
    newFieldData: [],
    noticies: {},
    addNewUser: false,
    userToAdd: "",
    usersForBoard: null,
    reloading: false,
    currentTimeStamp: 0,
    enteredHeader: false,
    notifications: null,
    showNotifications: false,
    currentNotificationTimeStamp: 0,
    otherUsers: {},
    canEditCheckbox: false,
    canEdit: false
  },
  methods: {
    init: function() {
      boardApp.loadBoard(function () {
        boardApp.loaded = true;
        boardApp.getTimeStamp(function (newTimeStamp) {
          boardApp.currentTimeStamp = newTimeStamp;
        });
        axios.get("/getOtherUsers").then(function (response) {
          boardApp.otherUsers = response.data;
        });
        axios.get("/canEdit", {params:{"whiteboard_id":getCurrentBoard()}})
        .then(function (response) {
          boardApp.canEdit = response.data;
        });
        boardApp.pingServer();
      });
    },
    pingServer: function() {
      boardApp.getTimeStamp(function (newTimeStamp) {
        if (boardApp.currentTimeStamp<newTimeStamp) {
          boardApp.reloading = true;
          boardApp.currentTimeStamp = newTimeStamp;
          boardApp.loadBoard(function () {});
        }
      });

      setTimeout(boardApp.pingServer, 1000);
    },
    addNewField: function() {
      boardApp.addField = !boardApp.addField;

      if (!boardApp.addField) {
        if (boardApp.whiteboard) {
          var boardToUse = boardApp.whiteboard;
        } else {
          var boardToUse = boardApp.sharedWhiteboard;
        }
        for (var i = 0; i < boardToUse.columns.split(",").length; i ++) {
          boardApp.newFieldData[i] = "";
        }
      }
    },
    createNotice: function() {
      var currentBoard = getCurrentBoard();
      var column_data = boardApp.newFieldData.reduce(function (accu, item) {
        return accu+","+item;
      }, "");
      boardApp.reloading = true;

      if (column_data.length>0)
        column_data = column_data.substring(1, column_data.length);

      var query = {"column_data":column_data, "whiteboard_id":currentBoard };

      axios.get("/createNotice", {params:query}).
      then(function(response) {
        boardApp.loadBoard(function () {
          boardApp.addField = false;
        });
      });
    },
    openEditor: function() {
      var query = {"whiteboard_id" : getCurrentBoard()};

      axios.get("/getUsersForBoard", {params:query})
      .then(function (response) {
        if (response.data)
          boardApp.usersForBoard = response.data;
        boardApp.addNewUser = true;
      });
    },
    closeEditor: function() {
      boardApp.addNewUser = false;
      boardApp.userToAdd = "";
      boardApp.canEditCheckbox = false;
    },
    addNewUserToBoard: function () {
      var query = {"username" : boardApp.userToAdd, "whiteboard_id" : getCurrentBoard(), "can_edit":boardApp.canEditCheckbox};
      axios.get("/addUserToWhiteboard", {params:query}).then(function (response) {
        window.location.reload();
      });
    },
    deleteUser: function(id) {
      axios.get("/deleteUserFromBoard", {params:{"user_id":id, "whiteboard_id": getCurrentBoard()}}).then(function (response) {
        window.location.reload();
      });
    },
    deleteWhiteBoard: function () {
      axios.get("/deleteBoard", {params:{"whiteboard_id":getCurrentBoard()}}).
      then(function (response) {
        window.location.href = "/";
      });
    },
    showCheck: function(item) {
      boardApp.noticies[item].showCheck = true;
      boardApp.$forceUpdate();
    },
    hideCheck: function(item) {
      boardApp.noticies[item].showCheck = false;
      boardApp.$forceUpdate();
    },
    toggleNotice: function (key) {
      var noticeToToggle = boardApp.noticies[key];
      boardApp.reloading = true;

      var query = {"notice_id":noticeToToggle.id};

      axios.get("/toggleNotice", {params:query}).then(function (response) {
        boardApp.loadBoard(function () {});
      });
    },
    deleteNotice: function(key) {
      var noticeToDelete = boardApp.noticies[key];
      boardApp.reloading = true;

      var query = {"whiteboard_id" : noticeToDelete.whiteboard_id, "notice_id":noticeToDelete.id};

      axios.get("/deleteNotice", {params:query}).then(function (response) {
        boardApp.loadBoard(function () {});
      });
    },
    getTimeStamp: function (afterPinged) {
      axios.get("/getNewestTimeStamp", {params:{"whiteboard_id" : getCurrentBoard()}})
      .then(function (response) {
        afterPinged(response.data);
      })
    },
    loadBoard: function (onComplete) {
      boardApp.reloading = true;
      var whiteboard = null;
      var sharedWhiteboard = null;
      var noticies = {};
      var newFieldData = [];

      var idOfBoard = getCurrentBoard();

      var query = {"whiteboard_id":idOfBoard};

      axios.get("/getWhiteboardNotices", {params:query}).then(function (response) {
        noticies = response.data;

        for (var k in noticies) {
          noticies[k].showCheck = false;
        }

        axios.get("/getNoticeChecks", {params:{"whiteboard_id": getCurrentBoard()}})
        .then(function (response) {
          var checkedNotices = response.data;

          for (var k in noticies) {
            for (var j in checkedNotices) {
              if (noticies[k].id == checkedNotices[j].notice_id) {
                noticies[k].checked = true;
              }
            }
          }
          boardApp.noticies = noticies;

          boardApp.$forceUpdate();
        });
      });

      axios.get("/getWhiteboards").then(function (response) {

        for (var k in response.data) {
          if (idOfBoard == response.data[k].id) {
            whiteboard = response.data[k];
          }
        }

        if (whiteboard) {
          for (var i = 0; i < whiteboard.columns.split(",").length; i ++)
            newFieldData.push("");

          boardApp.newFieldData = newFieldData;
          boardApp.whiteboard = whiteboard;
          boardApp.reloading = false;
          onComplete();
        }
      });

      axios.get("/getSharedWhiteboards").then(function (response) {

        for (var k in response.data) {
          if (idOfBoard == response.data[k].id) {
            sharedWhiteboard = response.data[k];
          }
        }

        if (sharedWhiteboard) {
          for (var i = 0; i < sharedWhiteboard.columns.split(",").length; i ++)
            newFieldData.push("");

          boardApp.newFieldData = newFieldData;
          boardApp.sharedWhiteboard = sharedWhiteboard;
          boardApp.reloading = false;
          onComplete();
        }
      });
    },
    enterHeader: function() {
      boardApp.enteredHeader = true;
    },
    exitHeader: function() {
      boardApp.enteredHeader = false;
    },
    toggleNotifications: function () {
      boardApp.showNotifications = !boardApp.showNotifications;
    },
    openNotification: function (notification_id, whiteboard_id) {
      axios.get("/closeNotifications", {params:{"notification_ids":notification_id}}).then(function (response) {
        if (whiteboard_id==getCurrentBoard()) {
          boardApp.getNotifications(function (notifications) {
            boardApp.notifications = notifications;
            boardApp.showNotifications = false;
          })
        } else {
          location.href="board?id="+whiteboard_id;
        }
      });
    },
    getNotifications: function(whenFound) {
      axios.get("/getNotifications")
      .then(function (response) {
        whenFound(response.data);
      });
    },
    pingServerForNotifications: function (whenFound) {
      axios.get("/getNotificationTimestamp").then(function (response) {
        whenFound(response.data);
      });
    },
    lookForNewNotifications: function() {
      boardApp.pingServerForNotifications(function (timestamp) {
        if (boardApp.currentNotificationTimeStamp < timestamp) {
          boardApp.currentNotificationTimeStamp = timestamp;

          boardApp.getNotifications(function (notifications) {
            boardApp.notifications = notifications;
          })
        }

        setTimeout(boardApp.lookForNewNotifications, 1000);
      });
    },
    selectUser: function(user) {
      boardApp.userToAdd = user;
    }
  },
  computed: {
    filteredNames: function() {
      var filteredUsers = [];

      if (this.userToAdd.length==0)
         return filteredUsers;

      var count = 0;
      for (var user in this.otherUsers) {
        if (this.otherUsers[user].username.toLowerCase().indexOf(this.userToAdd.toLowerCase())>=0) {
          if (count<5)
            if (this.otherUsers[user].username.toLowerCase() != this.userToAdd.toLowerCase()) {
              filteredUsers.push(this.otherUsers[user]);
              count++;
            }
        }
      }

      return filteredUsers;
    }
  }
});

var idOfBoard = -1;

function getCurrentBoard() {

  if (idOfBoard>=0)
    return idOfBoard;

  var queries = location.search;
  queries = queries.substring(1, queries.length);

  var queryArray = queries.split("&");
  idOfBoard = queryArray[0].split("=")[1];
  return idOfBoard;
}

boardApp.init();

boardApp.getNotifications(function (notifications) {
  boardApp.notifications = notifications;
})

boardApp.pingServerForNotifications(function (timestamp) {
  boardApp.currentNotificationTimeStamp = timestamp;
  boardApp.lookForNewNotifications();
});

document.getElementById("boardApp").classList.remove("initial");
