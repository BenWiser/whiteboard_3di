var registerApp = new Vue({
  el: "#registerApp",
  data: {
    username: "",
    password: "",
    confirmPassword: ""
  },
  methods: {
    register: function() {
      if (registerApp.username.length==0)
        return;

      if (registerApp.password.length==0)
        return;

      if (registerApp.password != registerApp.confirmPassword)
        return;

      var query = {"username": registerApp.username, "password" : registerApp.password};

      axios.get("/registerAccount", {params:query})
      .then(function (response) {
        if (response.data.success) {
          window.location.href = "/";
        }
      });
    }
  }
});
