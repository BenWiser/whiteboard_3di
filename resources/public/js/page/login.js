var loginApp = new Vue({
  el:"#loginApp",
  data: {
    username: "",
    password: ""
  },
  methods: {
    login: function() {

      var query = {'username': this.username, 'password': this.password};

      axios.get('/attemptLogin', {params:query})
      .then(function (response) {
        window.location.href = "/";
      });
    }
  }
});
