(ns whiteboard.core
  (:use [ring.util.response]
        [ring.middleware.resource]
        [ring.middleware.params]
        [ring.middleware.session])
  (:require [clojure.data.json :as json]
            [whiteboard.db :as db]
            [clojure.string :as str]
            [ring.adapter.jetty :as jetty])
  (:gen-class))

(declare checkForLogin)

(defn redirectTo [location]
  (redirect (str location)))

(defn respond [page]
  (resource-response (str page ".html") {:root "public"}))

(defn requestFunc [request uri func method privilege]
      (if (and (= (:request-method request) method) (= (str uri) (:uri request)))
          (content-type (checkForLogin request func privilege) "text/html")
          nil))

(defn GET [request uri privilege func]
  (requestFunc request uri func :get privilege))

(defn checkForLogin [request func privilege]
  (if (or privilege (contains? (:session request) :username))
    (func request)
    (redirectTo "/login")))

(defn attemptLogin [request]
 (let [params (:params request)]
   (let [username (params "username") password (params "password")]
      (if (db/userExists username password)
       (->
        (response (json/write-str {:username username :password password}))
        (assoc :session {:username username}))
       (response "failed")))))

(defn currentSession [request]
 (let [session (:session request)]
   (let [username (:username session)]
     (response (json/write-str {:username username})))))

(defn logout [request]
  (->
   (redirectTo "/")
   (assoc :session nil)))

(defn register [request]
  (let [params (:params request)]
    (let [username (params "username") password (params "password")]
      (db/createUser username password)))
  (response (json/write-str {:success true})))

(defn createWhiteboard [request]
  (let [params (:params request) session (:session request)]
    (let [username (:username session) tableName (params "tableName") columns (params "columns")]
      (db/createWhiteboard username tableName columns))))

(defn getWhiteboards [request]
  (let [session (:session request)]
    (let [username (:username session)]
      (response (json/write-str (db/getWhiteboards username))))))

(defn createNotice [request]
  (let [session (:session request) params (:params request)]
    (let [username (:username session) whiteboard_id (params "whiteboard_id") column_data (params "column_data")]
      (println (str whiteboard_id " " column_data))
      (db/createNotice whiteboard_id username column_data))))

(defn getWhiteboardNotices [request]
  (let [params (:params request)]
    (let [whiteboard_id (params "whiteboard_id")]
      (response (json/write-str (db/getNoticeById whiteboard_id))))))

(defn addUserToWhiteboard [request]
  (let [params (:params request)]
    (let [username (params "username") whiteboard_id (params "whiteboard_id") can_edit (params "can_edit")]
      (db/addUserToWhiteboard whiteboard_id username can_edit))))

(defn getUsersForBoard [request]
  (let [params (:params request)]
    (let [whiteboard_id (params "whiteboard_id")]
      (response (json/write-str (db/getUsersForBoard whiteboard_id))))))

(defn getSharedWhiteboards [request]
  (let [session (:session request)]
    (let [username (:username session)]
      (response (json/write-str (db/getSharedWhiteboards username))))))

(defn deleteUserFromBoard [request]
  (let [params (:params request)]
    (let [user_id (params "user_id") whiteboard_id (params "whiteboard_id")]
      (db/deleteUserFromBoard user_id whiteboard_id))))

(defn deleteBoard [request]
  (let [params (:params request)]
     (let [whiteboard_id (params "whiteboard_id")]
       (db/deleteBoard whiteboard_id)))
  (response "success"))

(defn deleteNotice [request]
  (let [params (:params request)]
    (let [notice_id (params "notice_id") whiteboard_id (params "whiteboard_id")]
      (db/deleteNotice notice_id whiteboard_id)))
  (response "success"))

(defn toggleNotice [request]
  (let [params (:params request) session (:session request)]
    (let [username (:username session) notice_id (params "notice_id")]
      (db/toggleNotice username notice_id))))

(defn getNoticeChecks [request]
  (let [params (:params request)]
    (let [whiteboard_id (params "whiteboard_id")]
      (response (json/write-str (db/getNoticeChecks whiteboard_id))))))

(defn getNewestTimeStamp [request]
  (let [params (:params request)]
    (let [whiteboard_id (params "whiteboard_id")]
      (response (json/write-str (db/getNewestTimeStamp whiteboard_id))))))

(defn getNotifications [request]
  (let [session (:session request)]
    (let [username (:username session)]
      (response (json/write-str (db/getAllNotifications username))))))

(defn closeNotifications [request]
  (let [params (:params request) session (:session request)]
    (let [notification_ids (params "notification_ids") username (:username session)]
      (let [ids (str/split notification_ids #",")]
        (db/closeNotifications ids username))))
  (response "success"))

(defn getNotificationTimestamp [request]
  (let [session (:session request)]
    (let [username (:username session)]
      (response (json/write-str (db/getNotificationTimestamp username))))))

(defn getOtherUsers [request]
  (let [session (:session request)]
    (let [username (:username session)]
      (response (json/write-str (db/getOtherUsers username))))))

(defn canEdit? [request]
  (let [session (:session request) params (:params request)]
    (let [username (:username session) whiteboard_id (params "whiteboard_id")]
      (response (json/write-str (db/canEdit? username whiteboard_id))))))

; All the routing

(defn handler [request]
  (or
    (GET request "/" false
      (fn [req] (redirectTo "/whiteboard")))
    (GET request "/login" true
      (fn [req] (respond "login")))
    (GET request "/register" true
      (fn [req] (respond "register")))
    (GET request "/whiteboard" false
      (fn [req] (respond "whiteboard")))
    (GET request "/attemptLogin" true
      attemptLogin)
    (GET request "/logout" true
      logout)
    (GET request "/currentSession" false
     currentSession)
    (GET request "/registerAccount" true
     register)
    (GET request "/createNewWhiteboard" false
      createWhiteboard)
    (GET request "/getWhiteboards" false
      getWhiteboards)
    (GET request "/board" false
      (fn [req] (respond "board")))
    (GET request "/createNotice" false
      createNotice)
    (GET request "/getWhiteboardNotices" false
      getWhiteboardNotices)
    (GET request "/addUserToWhiteboard" false
      addUserToWhiteboard)
    (GET request "/getUsersForBoard" false
      getUsersForBoard)
    (GET request "/getSharedWhiteboards" false
      getSharedWhiteboards)
    (GET request "/deleteUserFromBoard" false
      deleteUserFromBoard)
    (GET request "/deleteBoard" false
      deleteBoard)
    (GET request "/deleteNotice" false
      deleteNotice)
    (GET request "/toggleNotice" false
      toggleNotice)
    (GET request "/getNoticeChecks" false
      getNoticeChecks)
    (GET request "/getNewestTimeStamp" false
      getNewestTimeStamp)
    (GET request "/getNotifications" false
      getNotifications)
    (GET request "/closeNotifications" false
      closeNotifications)
    (GET request "/getNotificationTimestamp" false
      getNotificationTimestamp)
    (GET request "/getOtherUsers" false
      getOtherUsers)
    (GET request "/canEdit" false
      canEdit?)
    (respond "notFound")))


(def app
  (-> handler
   (wrap-resource "public")
   (wrap-params)
   (wrap-session)))
