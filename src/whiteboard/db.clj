(ns whiteboard.db
  (:require [clojure.java.jdbc :as sql]
            [digest]
            [clojure.string :as str])
  (:gen-class))

(def connection
    {:classname "org.h2.Driver"
     :subprotocol "h2:file"
     :subname "db/whiteboard"})

(declare getWhiteboardIdFromNotice logChange)

; For quickly clearing all the tables from repl
(defn dropTables []
  (sql/with-connection connection
     (sql/drop-table :user)
     (sql/drop-table :whiteboard)
     (sql/drop-table :notice)
     (sql/drop-table :whiteboardLinks)
     (sql/drop-table :checkedNotice)
     (sql/drop-table :change)
     (sql/drop-table :notification)))

; Generate tables if they don't exist
(defn tablesExist? []
  (try
    (sql/with-connection connection
      (sql/with-query-results res
        ["select * from user"]))
    (catch Exception e
      (sql/with-connection connection
        (sql/create-table :user
           [:id "bigint primary key auto_increment"]
           [:username "varchar(255)"]
           [:password "varchar(255)"])
        (sql/create-table :whiteboard
           [:id "bigint primary key auto_increment"]
           [:user_id "bigint"]
           [:tableName "varchar(255)"]
           [:columns "varchar(1000)"])
        (sql/create-table :notice
           [:id "bigint primary key auto_increment"]
           [:whiteboard_id "bigint"]
           [:user_id "bigint"]
           [:column_data "varchar(1000)"])
        (sql/create-table :whiteboardLinks
           [:id "bigint primary key auto_increment"]
           [:whiteboard_id "bigint"]
           [:user_id "bigint"]
           [:can_edit "boolean"])
        (sql/create-table :checkedNotice
           [:id "bigint primary key auto_increment"]
           [:user_id "bigint"]
           [:notice_id "bigint"])
        (sql/create-table :change
           [:id "bigint primary key auto_increment"]
           [:whiteboard_id "bigint"]
           [:event "varchar(1000)"]
           [:timestamp "bigint"])
        (sql/create-table :notification
           [:id "bigint primary key auto_increment"]
           [:change_id "bigint"]
           [:user_id "bigint"])))))

(defn getWhiteboardName [whiteboard_id]
  (let [results (sql/with-connection connection
                   (sql/with-query-results res
                       ["select tableName from whiteboard where id = ?" whiteboard_id] (doall res)))]
    (:tablename (nth results 0))))

(defn getWhiteboardIdFromNotice [notice_id]
  (let [results (sql/with-connection connection
                     (sql/with-query-results res
                         ["select whiteboard_id from notice where id = ?" notice_id] (doall res)))]
    (:whiteboard_id (nth results 0))))

(defn createUser [username password]
  (sql/with-connection connection
   (sql/insert-record :user
     {:username username :password (digest/md5 password)}))
  (println "user created " username))

(defn userExists [username password]
  (let [results (sql/with-connection connection
                 (sql/with-query-results res
                   ["select * from user where username = ? and password = ?" username (digest/md5 password)]
                   (doall res)))]
    (println (str "user logging in " username))
    (> (count results) 0)))

(defn getUserIdFirst [username]
  (let [results (sql/with-connection connection
                  (sql/with-query-results res
                    ["select id from user where username = ?" username]
                    (doall res)))]
    (:id (nth results 0))))

(def getUserId (memoize getUserIdFirst))

(defn- createWhiteboardById [ownerId tableName columns]
  (sql/with-connection connection
   (sql/insert-record :whiteboard
     {:user_id ownerId :tableName tableName :columns columns}))
  (println (str "Table " tableName " created")))

(defn createWhiteboard [username tableName columns]
  (createWhiteboardById (getUserId username) tableName columns))

(defn- getWhiteboardsFromId [ownerId]
  (let [results (sql/with-connection connection
                  (sql/with-query-results res
                   ["select id, tableName, columns from whiteboard where user_id = ?" ownerId] (doall res)))]
    results))

(defn- getSharedWhiteboardsFromId [id]
  (let [results (sql/with-connection connection
                  (sql/with-query-results res
                   [(str "select whiteboard.id, whiteboard.tableName, whiteboard.columns"
                         " from whiteboard join whiteboardLinks on "
                         " whiteboard.id = whiteboardLinks.whiteboard_id where"
                         " whiteboardLinks.user_id = ?") id] (doall res)))]
    results))


(defn- makeMap
  ([acc list counter]
   (if (empty? list)
     acc
     (recur (assoc acc counter (first list)) (rest list) (inc counter))))
  ([list]
   (makeMap {} list 0)))

(defn getSharedWhiteboards [username]
  (makeMap (getSharedWhiteboardsFromId (getUserId username))))

(defn getWhiteboards [username]
  (makeMap (getWhiteboardsFromId (getUserId username))))

(defn- createNoticeWithId [whiteboard_id user_id column_data]
  (sql/with-connection connection
   (sql/insert-record :notice
    {:whiteboard_id whiteboard_id :user_id user_id :column_data column_data}))
  (println (str "Notice has been created with data " column_data)))

(defn createNotice [whiteboard_id username column_data]
  (createNoticeWithId whiteboard_id (getUserId username) column_data)
  (logChange whiteboard_id (str "Row created on whiteboard " (getWhiteboardName whiteboard_id))))


(defn getNoticeById [whiteboard_id]
  (let [results (sql/with-connection connection
                                      (sql/with-query-results res
                                            ["select id, column_data, whiteboard_id from notice where whiteboard_id = ? " whiteboard_id] (doall res)))]

    (makeMap results)))

(defn- canEditById? [user_id whiteboard_id]
  (let [results (sql/with-connection connection
                   (sql/with-query-results res
                              ["select can_edit from whiteboardLinks where user_id = ? and whiteboard_id = ?" user_id whiteboard_id] (doall res)))]
    (:can_edit (nth results 0))))

(defn canEdit? [username whiteboard_id]
  (canEditById? (getUserId username) whiteboard_id))

(defn- addUserToWhiteboardById [whiteboard_id user_id can_edit]
  (and user_id (sql/with-connection connection
                (sql/insert-record :whiteboardLinks
                  {:whiteboard_id whiteboard_id :user_id user_id :can_edit can_edit})))
  (println (str "User " user_id " added to whiteboard " whiteboard_id)))

(defn addUserToWhiteboard [whiteboard_id username can_edit]
  (addUserToWhiteboardById whiteboard_id (getUserId username) can_edit))

(defn getUsersForBoard [whiteboard_id]
  (let [results (sql/with-connection connection
                                     (sql/with-query-results res
                                         [(str "select user.id, user.username, whiteboardLinks.can_edit from whiteboardLinks"
                                               " join whiteboard on whiteboardLinks.whiteboard_id = whiteboard.id"
                                               " join user on whiteboardLinks.user_id = user.id"
                                               " where whiteboard.id = ?") whiteboard_id] (doall res)))]
    (makeMap results)))

(defn deleteUserFromBoard [user_id whiteboard_id]
  (sql/with-connection connection
    (sql/delete-rows :whiteboardLinks
         ["user_id = ? and whiteboard_id = ?" user_id whiteboard_id]))
  (println (str "User " user_id " deleted from whiteboard " whiteboard_id)))

(defn deleteBoard [whiteboard_id]
  (sql/with-connection connection
   (sql/delete-rows :whiteboard
         ["id = ?" whiteboard_id]))
  (logChange whiteboard_id (str "Whiteboard " (getWhiteboardName whiteboard_id) " deleted"))
  (println (str "Whiteboard " whiteboard_id " removed")))

(defn deleteNotice [notice_id whiteboard_id]
  (sql/with-connection connection
    (sql/delete-rows :notice
      ["whiteboard_id = ? and id = ?" whiteboard_id notice_id]))
  (logChange whiteboard_id (str "Row deleted on whiteboard " (getWhiteboardName whiteboard_id)))
  (println (str "Notice " notice_id " deleted inside whiteboard " whiteboard_id)))

(defn- noticeCheckExists? [notice_id]
  (let [results (sql/with-connection connection
                    (sql/with-query-results res
                                            ["select * from checkedNotice where notice_id = ?" notice_id]
                                            (doall res)))]
    (> (count results) 0)))

(defn- deleteNoticeCheck! [notice_id]
  (sql/with-connection connection
                       (sql/delete-rows :checkedNotice
                                        ["notice_id = ?" notice_id]))
  (logChange (getWhiteboardIdFromNotice notice_id) (str "Notice unchecked on whiteboard " (getWhiteboardName (getWhiteboardIdFromNotice notice_id))))
  (println (str "Notice check for notice " notice_id " deleted")))

(defn- addNoticeCheck! [user_id notice_id]
  (sql/with-connection connection
                       (sql/insert-record :checkedNotice
                          {:user_id user_id :notice_id notice_id}))
  (logChange (getWhiteboardIdFromNotice notice_id) (str "Notice checked on whiteboard " (getWhiteboardName (getWhiteboardIdFromNotice notice_id))))
  (println (str "Notice check for notice " notice_id " added")))

(defn- toggleNoticeById [user_id notice_id]
  (if (noticeCheckExists? notice_id)
    (deleteNoticeCheck! notice_id)
    (addNoticeCheck! user_id notice_id)))

(defn toggleNotice [username notice_id]
  (toggleNoticeById (getUserId username) notice_id))

(defn getNoticeChecks [whiteboard_id]
  (let [results (sql/with-connection connection
                    (sql/with-query-results res
                                            [(str "select checkedNotice.notice_id from checkedNotice"
                                                  " join notice on checkedNotice.notice_id = notice.id "
                                                  " where notice.whiteboard_id = ?") whiteboard_id]
                                            (doall res)))]
    (makeMap results)))

(defn- getWhiteboardIdFromNotice [notice_id]
  (let [results (sql/with-connection connection
                    (sql/with-query-results res
                                            [(str "select whiteboard_id from notice "
                                                  "where id = ?") notice_id] (doall res)))]
    (:whiteboard_id (nth results 0))))

(defn- logChange [whiteboard_id event]
  (sql/with-connection connection
     (sql/insert-record :change
        {:whiteboard_id whiteboard_id :event event :timestamp (System/currentTimeMillis)})))

(defn getNewestTimeStamp [whiteboard_id]
  (let [results (sql/with-connection connection
                  (sql/with-query-results res
                                          [(str "select timestamp from change "
                                                "where id = (select max(id) from change where "
                                                "whiteboard_id = ?)") whiteboard_id]
                                          (doall res)))]
    (:timestamp (nth results 0))))

(defn- getNotificationTimestampById [user_id]
  (let [results (sql/with-connection connection
                   (sql/with-query-results res
                                           [(str "select timestamp from change "
                                                 "where id = "
                                                 "(select max(id) from (select id "
                                                 "from change where whiteboard_id in "
                                                 "(select id from whiteboard where user_id = ?) "
                                                 "and id not in "
                                                 "(select change_id from notification where user_id = ?) "
                                                 "UNION "
                                                 "select id "
                                                 "from change where whiteboard_id in "
                                                 "(select whiteboard_id from whiteboardLinks where user_id = ?) "
                                                 "and id not in "
                                                 "(select change_id from notification where user_id = ?) ) ) ") user_id user_id user_id user_id]
                                           (doall res)))]
    (:timestamp (nth results 0))))

(defn- getAllNotificationsById [user_id]
  (let [results (sql/with-connection connection
                   (sql/with-query-results res
                                           [(str "select id, event, whiteboard_id "
                                                 "from change where whiteboard_id in "
                                                 "(select id from whiteboard where user_id = ?) "
                                                 "and id not in "
                                                 "(select change_id from notification where user_id = ?)"
                                                 "UNION "
                                                 "select id, event, whiteboard_id "
                                                 "from change where whiteboard_id in "
                                                 "(select whiteboard_id from whiteboardLinks where user_id = ?) "
                                                 "and id not in "
                                                 "(select change_id from notification where user_id = ?) "
                                                 "order by id desc") user_id user_id user_id user_id]
                                           (doall res)))]
    results))

(defn getAllNotifications [username]
  (getAllNotificationsById (getUserId username)))

(defn getNotificationTimestamp [username]
  (getNotificationTimestampById (getUserId username)))

(defn- addNotification [change_id user_id]
    (sql/with-connection connection
     (sql/insert-record :notification
       {:change_id change_id :user_id user_id})))

(defn closeNotifications [notification_ids username]
    (let [user_id (getUserId username)]
       (doall (map (fn [change_id] (addNotification change_id user_id)) notification_ids))))

(defn- getOtherUsersById [user_id]
  (let [results (sql/with-connection connection
                    (sql/with-query-results res
                           ["select username from user where id <> ? and username is not null" user_id] (doall res)))]
    (makeMap results)))

(defn getOtherUsers [username]
  (getOtherUsersById (getUserId username)))
