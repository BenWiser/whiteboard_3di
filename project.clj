(defproject whiteboard "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [ring/ring-core "1.5.0"]
                 [ring/ring-jetty-adapter "1.5.0"]
                 [org.clojure/java.jdbc "0.3.6"]
                 [org.clojure/data.json "0.2.6"]
                 [org.clojure/java.jdbc "0.2.3"]
                 [com.h2database/h2 "1.3.170"]
                 [digest "1.4.5"]]
  :plugins [[lein-ring "0.9.7"]]
  :ring {:handler whiteboard.core/app
         :init whiteboard.db/tablesExist?
	 :port 80})
